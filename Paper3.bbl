\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }

\bibitem{Alilou}
Alilou, M., Kovalev, V.: Automatic object detection and segmentation of the
  histocytology images using reshapable agents. Image Analysis and Stereology
  32(2),  89--99 (2013)

\bibitem{Alomari}
Alomari, Y.M., Sheikh~Abdullah, S.N.H., Zaharatul~Azma, R., Omar, K.: Automatic
  detection and quantification of wbcs and rbcs using iterative structured
  circle detection algorithm. Computational and mathematical methods in
  medicine  2014 (2014)

\bibitem{Put15c}
{Di Ruberto}, C., Loddo, A., Putzu, L.: Learning by sampling for white blood
  cells segmentation. In: ICIAP International Conference on Image Analysis and
  Processing, Lecture Notes in Computer Science, vol. 9279, pp. 557--567.
  Springer International Publishing (2015)

\bibitem{Put15d}
{Di Ruberto}, C., Loddo, A., Putzu, L.: A multiple classifier learning by
  sampling system for white blood cells segmentation. In: International
  Conference CAIP on Computer Analysis of Images and Patterns, Lecture Notes in
  Computer Science, vol. 9257, pp. 415--425. Springer International Publishing
  (2015)

\bibitem{Put14a}
{Di Ruberto}, C., Putzu, L.: Accurate blood cells segmentation through
  intuitionistic fuzzy set threshold. In: International Conference SITIS on
  Signal-Image Technology and Internet-Based Systems. pp. 57--64 (Nov 2014)

\bibitem{fukunaga}
Fukunaga, K., Hostetler, L.: The estimation of the gradient of a density
  function, with applications in pattern recognition. IEEE Transactions on
  Information Theory  21(1),  32--40 (Jan 1975)

\bibitem{Khan}
Khan, S., Khan, A., Khattak, F.S., Naseem, A.: An accurate and cost effective
  approach to blood cell count. International Journal of Computer Applications
  50(1) (2012)

\bibitem{Kovalev}
Kovalev, V.A., Grigoriev, A.Y., Hyo-Sok, A.: Robust recognition of white blood
  cell images. In: International Conference on Pattern Recognition. vol.~4, pp.
  371--375 (Aug 1996)

\bibitem{Donida}
Labati, R.D., Piuri, V., Scotti, F.: All-idb: The acute lymphoblastic leukemia
  image database for image processing. In: IEEE ICIP International Conference
  on Image Processing. pp. 2045--2048 (Sept 2011)

\bibitem{Madhloom}
Madhloom, H.T., Kareem, S.A., Ariffin, H., Zaidan, A.A., Alanazi, H.O., Zaidan,
  B.B.: An automated white blood cell nucleus localization and segmentation
  using image arithmetic and automatic threshold. Journal of Applied Sciences
  10(11),  959--966 (2010)

\bibitem{Mahmood}
Mahmood, N.H., Lim, P.C., Mazalan, S.M., Razak, M.A.A.: Blood cells extraction
  using color based segmentation technique. International Journal of Life
  Sciences Biotechnology and Pharma Research  2(2) (2013)

\bibitem{Mohamed}
Mohamed, M., Far, B., Guaily, A.: An efficient technique for white blood cells
  nuclei automatic segmentation. In: IEEE International Conference on Systems,
  Man, and Cybernetics (SMC). pp. 220--225 (Oct 2012)

\bibitem{Nguyen}
Nguyen, N.T., Duong, A.D., Vu, H.Q.: Cell splitting with high degree of
  overlapping in peripheral blood smear. International Journal of Computer
  Theory and Engineering  3(3),  473 (2011)

\bibitem{pan}
Pan, C., Lu, H., Cao, F.: Segmentation of blood and bone marrow cell images via
  learning by sampling. In: Emerging Intelligent Computing Technology and
  Applications, Lecture Notes in Computer Science, vol. 5754, pp. 336--345.
  Springer Berlin Heidelberg (2009)

\bibitem{Put14b}
Putzu, L., Caocci, G., {Di Ruberto}, C.: Leucocyte classification for leukaemia
  detection using image processing techniques. Artificial Intelligence in
  Medicine  62(3),  179--191 (2014)

\bibitem{Sarrafzadeh}
Sarrafzadeh, O., Rabbani, H., Talebi, A., Banaem, H.U.: Selection of the best
  features for leukocytes classification in blood smear microscopic images
  (2014)

\bibitem{Sco06}
Scotti, F.: Robust segmentation and measurements techniques of white cells in
  blood microscope images. In: IEEE IMTC Instrumentation and Measurement
  Technology Conference. pp. 43--48 (April 2006)

\bibitem{shapiro}
Shapiro, L.G., Stockman, G.: Computer Vision. Prentice Hall PTR, 1st edn.
  (2001)

\bibitem{Sinha}
Sinha, N., Ramakrishnan, A.G.: Automation of differential blood count. In:
  TENCON Conference on Convergent Technologies for the Asia-Pacific Region.
  vol.~2, pp. 547--551 (Oct 2003)

\bibitem{Wilkinson}
Wilkinson, M.H.F.: Shading correction and calibration in bacterial fluorescence
  measurement by image processing system. Comp. Meth. Prog. Biomed.  44,
  61--67 (1994)

\end{thebibliography}
